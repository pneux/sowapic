<?php

namespace Pneux\Sowapic\Tests;

use Dotenv\Dotenv;
use PHPUnit\Framework\TestCase;
use Pneux\Sowapic\OpenWeatherApiClient;
use Pneux\Sowapic\Response\WeatherForecast;

class ClientTest extends TestCase
{
    public function testClient(): void
    {
        // CONSIDERING...
        $dotenv = Dotenv::createImmutable(__DIR__ . '/..');
        $dotenv->safeLoad();
        $apiKey = $_ENV['API_KEY_TEST'];
        $client = new OpenWeatherApiClient($apiKey);

        // WHEN...
        $currentWeather = $client->getCurrentWeatherByCityID(3067696);
        $weatherForecast = $client->getWeatherForecast(3067696);

        $weatherForecast->forEach(function (WeatherForecast $forecast) {
            var_dump($forecast);
        });

        // THEN...
    }
}