<?php

namespace Pneux\Sowapic\Response;

use DateTime;
use DateTimeInterface;
use stdClass;

final class WeatherForecast extends Weather
{
    private float             $precipitationProbability;
    private DateTimeInterface $forecastedTime;

    public static function createFromJsonObject(stdClass $obj): self
    {
        $main = $obj->main;
        $wind = $obj->wind;
        $rain = $obj->rain ?? null;
        $snow = $obj->snow ?? null;

        return new self(
            $main->temp,
            $main->feels_like,
            $main->pressure,
            $main->humidity,
            $obj->visibility,
            $wind->speed,
            $wind->deg,
            $wind->gust ?? null,
            $obj->clouds->all,
            $rain?->{'3h'} ?? null,
            $snow?->{'3h'} ?? null,
            $obj->pop,
            $obj->weather,
            DateTime::createFromFormat('U', $obj->dt)
        );
    }

    /**
     * @param float $temperature
     * @param float $feelsLikeTemperature
     * @param int $pressure
     * @param int $humidity
     * @param int $visibility
     * @param float $windSpeed
     * @param int $windDirection
     * @param float|null $windGustSpeed
     * @param int $cloudiness
     * @param float|null $rainVolumeLast3Hours
     * @param float|null $snowVolumeLast3Hours
     * @param float $precipitationProbability
     * @param stdClass[] $weatherConditions
     * @param DateTimeInterface $forecastedTime
     */
    private function __construct(
        float             $temperature,
        float             $feelsLikeTemperature,
        int               $pressure,
        int               $humidity,
        int               $visibility,
        float             $windSpeed,
        int               $windDirection,
        ?float            $windGustSpeed,
        int               $cloudiness,
        ?float            $rainVolumeLast3Hours,
        ?float            $snowVolumeLast3Hours,
        float             $precipitationProbability,
        array             $weatherConditions,
        DateTimeInterface $forecastedTime,
    ) {
        parent::__construct(
            $temperature,
            $feelsLikeTemperature,
            $pressure,
            $humidity,
            $visibility,
            $windSpeed,
            $windDirection,
            $windGustSpeed,
            $cloudiness,
            $rainVolumeLast3Hours,
            $snowVolumeLast3Hours,
            $weatherConditions,
        );

        $this->forecastedTime = $forecastedTime;
        $this->precipitationProbability = $precipitationProbability;
    }

    /**
     * @return float
     */
    public function getPrecipitationProbability(): float
    {
        return $this->precipitationProbability;
    }

    /**
     * @return DateTimeInterface
     */
    public function getForecastedTime(): DateTimeInterface
    {
        return $this->forecastedTime;
    }
}