<?php

namespace Pneux\Sowapic\Response;

use DateTime;
use DateTimeInterface;
use stdClass;

final class CurrentWeather extends Weather
{
    private ?float            $rainVolumeLastHour;
    private ?float            $snowVolumeLastHour;
    private DateTimeInterface $updatedAt;

    public static function createFromJsonResponse(string $jsonResponse): self
    {
        /** @var stdClass $data */
        $data = json_decode($jsonResponse);

        $main = $data->main;
        $wind = $data->wind;
        $rain = $data->rain ?? null;
        $snow = $data->snow ?? null;

        return new self(
            $main->temp,
            $main->feels_like,
            $main->pressure,
            $main->humidity,
            $data->visibility,
            $wind->speed,
            $wind->deg,
            $wind->gust ?? null,
            $data->clouds->all,
            $rain?->{'1h'} ?? null,
            $rain?->{'3h'} ?? null,
            $snow?->{'1h'} ?? null,
            $snow?->{'3h'} ?? null,
            $data->weather,
            DateTime::createFromFormat('U', $data->dt)
        );
    }

    /**
     * @param float $temperature
     * @param float $feelsLikeTemperature
     * @param int $pressure
     * @param int $humidity
     * @param int $visibility
     * @param float $windSpeed
     * @param int $windDirection
     * @param float|null $windGustSpeed
     * @param int $cloudiness
     * @param float|null $rainVolumeLastHour
     * @param float|null $rainVolumeLast3Hours
     * @param float|null $snowVolumeLastHour
     * @param float|null $snowVolumeLast3Hours
     * @param stdClass[] $weatherConditions
     * @param DateTimeInterface $updatedAt
     */
    private function __construct(
        float             $temperature,
        float             $feelsLikeTemperature,
        int               $pressure,
        int               $humidity,
        int               $visibility,
        float             $windSpeed,
        int               $windDirection,
        ?float            $windGustSpeed,
        int               $cloudiness,
        ?float            $rainVolumeLastHour,
        ?float            $rainVolumeLast3Hours,
        ?float            $snowVolumeLastHour,
        ?float            $snowVolumeLast3Hours,
        array             $weatherConditions,
        DateTimeInterface $updatedAt,
    ) {
        parent::__construct(
            $temperature,
            $feelsLikeTemperature,
            $pressure,
            $humidity,
            $visibility,
            $windSpeed,
            $windDirection,
            $windGustSpeed,
            $cloudiness,
            $rainVolumeLast3Hours,
            $snowVolumeLast3Hours,
            $weatherConditions,
        );

        $this->rainVolumeLastHour = $rainVolumeLastHour;
        $this->snowVolumeLastHour = $snowVolumeLastHour;
        $this->updatedAt          = $updatedAt;
    }

    /**
     * @return float|null
     */
    public function getRainVolumeLastHour(): ?float
    {
        return $this->rainVolumeLastHour;
    }

    /**
     * @return float|null
     */
    public function getSnowVolumeLastHour(): ?float
    {
        return $this->snowVolumeLastHour;
    }

    /**
     * @return DateTimeInterface
     */
    public function getUpdatedAt(): DateTimeInterface
    {
        return $this->updatedAt;
    }
}