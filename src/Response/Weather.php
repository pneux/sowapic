<?php

namespace Pneux\Sowapic\Response;

use stdClass;

abstract class Weather
{
    protected float  $temperature;
    protected float  $feelsLikeTemperature;
    protected int    $pressure;
    protected int    $humidity;
    protected int    $visibility;
    protected float  $windSpeed;
    protected int    $windDirection;
    protected ?float $windGustSpeed;
    protected int    $cloudiness;
    protected ?float $rainVolumeLast3Hours;
    protected ?float $snowVolumeLast3Hours;

    /** @var string[] */
    protected array  $weatherConditions;

    /**
     * @param float $temperature
     * @param float $feelsLikeTemperature
     * @param int $pressure
     * @param int $humidity
     * @param int $visibility
     * @param float $windSpeed
     * @param int $windDirection
     * @param float|null $windGustSpeed
     * @param int $cloudiness
     * @param float|null $rainVolumeLast3Hours
     * @param float|null $snowVolumeLast3Hours
     * @param stdClass[] $weatherConditions
     */
    protected function __construct(
        float $temperature,
        float $feelsLikeTemperature,
        int $pressure,
        int $humidity,
        int $visibility,
        float $windSpeed,
        int $windDirection,
        ?float $windGustSpeed,
        int $cloudiness,
        ?float $rainVolumeLast3Hours,
        ?float $snowVolumeLast3Hours,
        array $weatherConditions
    ) {
        $this->temperature          = $temperature;
        $this->feelsLikeTemperature = $feelsLikeTemperature;
        $this->pressure             = $pressure;
        $this->humidity             = $humidity;
        $this->visibility           = $visibility;
        $this->windSpeed            = $windSpeed;
        $this->windDirection        = $windDirection;
        $this->windGustSpeed        = $windGustSpeed;
        $this->cloudiness           = $cloudiness;
        $this->rainVolumeLast3Hours = $rainVolumeLast3Hours;
        $this->snowVolumeLast3Hours = $snowVolumeLast3Hours;
        $this->weatherConditions    = array_map(fn($weather) => $weather->description, $weatherConditions);
    }

    /**
     * @return float
     */
    public function getTemperature(): float
    {
        return $this->temperature;
    }

    /**
     * @return float
     */
    public function getFeelsLikeTemperature(): float
    {
        return $this->feelsLikeTemperature;
    }

    /**
     * @return int
     */
    public function getPressure(): int
    {
        return $this->pressure;
    }

    /**
     * @return int
     */
    public function getHumidity(): int
    {
        return $this->humidity;
    }

    /**
     * @return int
     */
    public function getVisibility(): int
    {
        return $this->visibility;
    }

    /**
     * @return float
     */
    public function getWindSpeed(): float
    {
        return $this->windSpeed;
    }

    /**
     * @return int
     */
    public function getWindDirection(): int
    {
        return $this->windDirection;
    }

    /**
     * @return float|null
     */
    public function getWindGustSpeed(): ?float
    {
        return $this->windGustSpeed;
    }

    /**
     * @return int
     */
    public function getCloudiness(): int
    {
        return $this->cloudiness;
    }

    /**
     * @return float|null
     */
    public function getRainVolumeLast3Hours(): ?float
    {
        return $this->rainVolumeLast3Hours;
    }

    /**
     * @return float|null
     */
    public function getSnowVolumeLast3Hours(): ?float
    {
        return $this->snowVolumeLast3Hours;
    }

    /**
     * @return string[]
     */
    public function getWeatherConditions(): array
    {
        return $this->weatherConditions;
    }
}