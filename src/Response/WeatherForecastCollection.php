<?php

namespace Pneux\Sowapic\Response;

use stdClass;

class WeatherForecastCollection
{
    private int $cityID;

    /** @var WeatherForecast[] */
    private array $forecasts;

    public function __construct(string $jsonResponse)
    {
        /** @var stdClass $data */
        $data = json_decode($jsonResponse);

        $this->cityID = $data->city->id;

        $this->forecasts = array_map(function ($listItem) {
            return WeatherForecast::createFromJsonObject($listItem);
        }, $data->list);
    }

    public function forEach(\Closure $closure): void
    {
        foreach ($this->forecasts as $forecast) {
            $closure($forecast);
        }
    }

    /**
     * @return int
     */
    public function getCityID(): int
    {
        return $this->cityID;
    }
}