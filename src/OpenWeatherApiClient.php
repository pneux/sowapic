<?php

namespace Pneux\Sowapic;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Pneux\Sowapic\Response\CurrentWeather;
use Pneux\Sowapic\Response\WeatherForecastCollection;

class OpenWeatherApiClient
{
    private string $apiKey;
    private Client $client;

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;

        $this->client = new Client([
            'base_uri' => 'https://api.openweathermap.org',
            'timeout'  => 3.0,
        ]);
    }

    /**
     * @param int $cityID
     *
     * @return CurrentWeather
     *
     * @throws GuzzleException
     */
    public function getCurrentWeatherByCityID(int $cityID): CurrentWeather
    {
        $response = $this->client->get('/data/2.5/weather', [
            'query' => [
                'appid' => $this->apiKey,
                'id'    => $cityID,
                'units' => 'metric',
            ]
        ]);

        return CurrentWeather::createFromJsonResponse((string) $response->getBody());
    }

    /**
     * @param int $cityID
     *
     * @return WeatherForecastCollection
     *
     * @throws GuzzleException
     */
    public function getWeatherForecast(int $cityID): WeatherForecastCollection
    {
        $response = $this->client->get('/data/2.5/forecast', [
            'query' => [
                'appid' => $this->apiKey,
                'id'    => $cityID,
                'units' => 'metric',
            ]
        ]);

        return new WeatherForecastCollection((string) $response->getBody());
    }
}